//
//  ViewController.h
//  hipChat
//
//  Created by Micheal Cumming on 11/02/2016.
//  Copyright © 2016 Micheal Cumming. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *inputTextField;
@property (weak, nonatomic) IBOutlet UITextView *decodedOutput;

@property (strong) NSMutableArray *mentions;
@property (strong) NSMutableArray *emoticons;
@property (strong) NSMutableArray *links;

-(void)processInputText:(NSString*)inputText;

@end

