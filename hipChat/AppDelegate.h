//
//  AppDelegate.h
//  hipChat
//
//  Created by Micheal Cumming on 11/02/2016.
//  Copyright © 2016 Micheal Cumming. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

