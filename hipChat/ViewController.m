//
//  ViewController.m
//  hipChat
//
//  Created by Micheal Cumming on 11/02/2016.
//  Copyright © 2016 Micheal Cumming. All rights reserved.
//

/*
 
 DESIGN BRIEF
 
 Please write an app that allows a user to input a chat message string and convert it into a JSON string containing information about its contents. Special content to look for includes:
 1. @mentions - A way to mention a user. Always starts with an '@' and ends when hitting a non-word character. (http://help.hipchat.com/knowledgebase/articles/64429-how-do-mentions-work-)
 2. Emoticons - For this exercise, you only need to consider 'custom' emoticons which are alphanumeric strings, no longer than 15 characters, contained in parenthesis. You can assume that anything matching this format is an emoticon. (https://www.hipchat.com/emoticons)
 3. Links - Any URLs contained in the message, along with the page's title.
 
 ASSUMPTIONS
 
 1. That the 'space' is the most expected delimiter and used as the non-word character.
 2. Links that are badly formed or take longer the timeout period to load will be ignored.
 3. HTML provided from server has tags in lowercase.
 4. Special characters in page title are not decoded to ensure valid JSON.
 5. URL redirections are not handled and treated as bad link.
 
 ALGORITHM
 
 The algorithm to be used will be as follows;
 1. Break the text into segments using 'space' as a delimiter.
 2. Process each segment and determine if it contains special content.
 3. Fetch details for any links encountered.
 4. Compile arrays of special content into dictionary.
 5. Convert dictionary to JSON and return output.

 PROVIDED TEST EXAMPLES
 
 Input: "@chris you around?"
 Return (string):
 {
    "mentions": [
    "chris"
    ]
 }
 
 Input: "Good morning! (megusta) (coffee)"
 Return (string):
 {
    "emoticons": [
        "megusta",
        "coffee"
    ]
 }
 
 Input: "Olympics are starting soon; http://www.nbcolympics.com"
 Return (string):
 {
    "links": [
        {
        "url": "http://www.nbcolympics.com",
        "title": "NBC Olympics | 2014 NBC Olympics in Sochi Russia"
        }
    ]
 }
 
 Input: "@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016"
 Return (string):
 {
    "mentions": [
        "bob",
        "john"
    ],
    "emoticons": [
        "success"
    ],
    "links": [
        {
            "url": "https://twitter.com/jdorfman/status/430511497475670016",
            "title": "Twitter / jdorfman: nice @littlebigdetail from ..."
        }
    ]
 }

*/

#import "ViewController.h"

#define TIMEOUT 5

@interface ViewController ()
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Setup UI
    self.inputTextField.delegate = self; // Set up delegate to allow us to see when user pressed RETURN key
    [self.decodedOutput setText:@""];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark TextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == self.inputTextField) {
        [textField resignFirstResponder]; // Make the keyboard hide so you can see the rest of the screen
        [self processInputText:textField.text]; // Process the input text
        return NO;
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    // Clear Input textfield when entering field to start new decoding session
    textField.text = @"";
    [self.decodedOutput setText:@""];
}

#pragma mark ProcessInputText

-(void)processInputText:(NSString*)inputText
{
    NSLog(@"Input: %@",inputText);
    
    // Initialise Data Structures
    self.mentions = [[NSMutableArray alloc] init];
    self.emoticons = [[NSMutableArray alloc] init];
    self.links = [[NSMutableArray alloc] init];
    
    //1. Break the text into segments using the space as a delimiter.
    
    NSArray *segments = [inputText componentsSeparatedByString:@" "];
    
    //2. Process each segment and determine if it contains special content.
    
    for (NSString *segment in segments)
    {
        NSString *lowercaseSegment = [segment lowercaseString]; //used for decoding case-insensitive links
        
        if (([segment length]>1) && ([[segment substringToIndex:1] isEqualToString:@"@"]))
        {
            // Found a Mention
            NSString *mention = [segment substringFromIndex:1];
            [_mentions addObject:mention];
            
        }else if (([segment length]>2 && [segment length]<18) && ([[segment substringToIndex:1] isEqualToString:@"("] && [[segment substringFromIndex:[segment length]-1] isEqualToString:@")"]))
        {
            // Found a Emoticon less then 15 characters Long
            NSString * emoticon = [[segment substringFromIndex:1] substringToIndex:[segment length]-2];
            [_emoticons addObject:emoticon];
            
        }else if (([lowercaseSegment length] > 8) && ([[lowercaseSegment substringToIndex:7] isEqualToString:@"http://"] || [[lowercaseSegment substringToIndex:8] isEqualToString:@"https://"]))
        {
            // Found a Link
            
            //3. Fetch details for any links encountered.
            NSDictionary * link = [self getDetailsFromLink:lowercaseSegment];
            if (link) [_links addObject:link];
            
        }
    }
    
    //4. Compile arrays of special content into dictionary.
    
    NSMutableDictionary * dictionary = [[NSMutableDictionary alloc] init];
    
    if([_mentions count]>0)[dictionary setObject:_mentions forKey:@"mentions"];
    if([_emoticons count]>0)[dictionary setObject:_emoticons forKey:@"emoticons"];
    if([_links count]>0)[dictionary setObject:_links forKey:@"links"];
    
    //5. Convert dictionary to JSON and return output.

    NSError *error;
    NSString *jsonString;
    
    @try {
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:&error];
        
        if (jsonData) {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSASCIIStringEncoding];
        } else {
            jsonString = @"There was an error encoding JSON.";
        }
    }
    @catch (NSException *exception) {
        jsonString = [NSString stringWithFormat:@"There was an exception encoding JSON. %@",[error localizedDescription]];
    }
 
    //Sanitise string from bug/feature with NSJSONSerialsation that escapes "/"
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\\/" withString:@"/"];

    NSLog(@"Return(string):\n%@",jsonString);
    
    [_decodedOutput setText:jsonString];
    
}

-(NSDictionary*)getDetailsFromLink:(NSString*)link
{
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:link]];
    
    // Create a SYNCRONOUS request using a semaphore
    
    dispatch_semaphore_t semaphore;
    __block NSData * result;
    
    result = nil;
    semaphore = dispatch_semaphore_create(0);
    
    // Run on global queue to get ability to use a semaphore
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    
        [[[NSURLSession sharedSession] dataTaskWithRequest:request
                                         completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                            
                                             if (error == nil) {  
                                                 result = data;
                                             }
                                             else
                                             {
                                                 NSLog(@"ERROR: %@",[error localizedDescription]);
                                             }
                                             
                                             dispatch_semaphore_signal(semaphore); // Used to tell dispatch_semaphone_wait to continue
                                             
                                         }] resume];
    });
    
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER); //Wait until a response from NSURLSession either a result, an error, or a timeout.
    
    NSString *htmlString = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    
    __block NSString * title = [[NSString alloc] init];
    
    // Extract the title from the returned HTML
    NSRegularExpression* myRegex = [[NSRegularExpression alloc] initWithPattern:@"<title>(.*)</title>" options:0 error:nil];
    [myRegex enumerateMatchesInString:htmlString options:0 range:NSMakeRange(0, [htmlString length]) usingBlock:^(NSTextCheckingResult *match, NSMatchingFlags flags, BOOL *stop) {
        NSRange range = [match rangeAtIndex:1];
        title =[htmlString substringWithRange:range];
    }];
    
    if ([title length] > 0) {
        // Create a dictionary to hold link data
        NSMutableDictionary * details = [[NSMutableDictionary alloc] init];
        [details setObject:link forKey:@"url"];
        [details setObject:title forKey:@"title"];
        return details;
    }

    return nil;
}

@end
