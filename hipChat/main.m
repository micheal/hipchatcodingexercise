//
//  main.m
//  hipChat
//
//  Created by Micheal Cumming on 11/02/2016.
//  Copyright © 2016 Micheal Cumming. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
