//
//  hipChatTests.m
//  hipChatTests
//
//  Created by Micheal Cumming on 11/02/2016.
//  Copyright © 2016 Micheal Cumming. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ViewController.h"

@interface hipChatTests : XCTestCase

@property (nonatomic, strong) ViewController *viewController;

@end

@implementation hipChatTests

- (void)setUp {
    [super setUp];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    self.viewController = [storyboard instantiateViewControllerWithIdentifier:@"main"];
    [self.viewController performSelectorOnMainThread:@selector(loadView) withObject:nil waitUntilDone:YES];
    
}

- (void)tearDown {
    self.viewController = nil;
    [super tearDown];
}

- (void)testInitNotNil
{
    XCTAssertNotNil(self.viewController, @"Test 'ViewController' object not instantiated");
}

- (void)testExampleOne {

    // Testing provided example 1
    
    NSString * example = @"@chris you around?";
    [_viewController processInputText:example];
    
    NSString * expectation = @"chris";
    NSString * returned = [_viewController.mentions objectAtIndex:0];
    
    XCTAssertTrue([[_viewController.mentions objectAtIndex:0] isEqualToString:expectation],@"Process Input returned %@ but %@ was expected.",expectation,returned);
    
    int expectation1 = 1;
    int returned1 = [_viewController.mentions count];
    
    int expectation2 = 0;
    int returned2 = [_viewController.emoticons count];
    
    int expectation3 = 0;
    int returned3 = [_viewController.links count];
    
    XCTAssertTrue(expectation1 == returned1,@"Process Input #1 returned %d but %d was expected.",expectation1,returned1);
    XCTAssertTrue(expectation2 == returned2,@"Process Input #2 returned %d but %d was expected.",expectation2,returned2);
    XCTAssertTrue(expectation3 == returned3,@"Process Input #2 returned %d but %d was expected.",expectation3,returned3);
    
}

- (void)testExampleTwo {

    // Testing provided example 2
    
    NSString * example = @"Good morning! (megusta) (coffee)";
    [_viewController processInputText:example];
    
    int expectation1 = 0;
    int returned1 = [_viewController.mentions count];
    
    int expectation2 = 2;
    int returned2 = [_viewController.emoticons count];
    
    int expectation3 = 0;
    int returned3 = [_viewController.links count];
    
    XCTAssertTrue(expectation1 == returned1,@"Process Input #1 returned %d but %d was expected.",expectation1,returned1);
    XCTAssertTrue(expectation2 == returned2,@"Process Input #2 returned %d but %d was expected.",expectation2,returned2);
    XCTAssertTrue(expectation3 == returned3,@"Process Input #2 returned %d but %d was expected.",expectation3,returned3);

    
}

- (void)testExampleThree {
    
    // Testing provided example 3
    
    NSString * example = @"Olympics are starting soon; http://www.nbcolympics.com";
    [_viewController processInputText:example];
    
    int expectation1 = 0;
    int returned1 = [_viewController.mentions count];
    
    int expectation2 = 0;
    int returned2 = [_viewController.emoticons count];
    
    int expectation3 = 1;
    int returned3 = [_viewController.links count];
    
    XCTAssertTrue(expectation1 == returned1,@"Process Input #1 returned %d but %d was expected.",expectation1,returned1);
    XCTAssertTrue(expectation2 == returned2,@"Process Input #2 returned %d but %d was expected.",expectation2,returned2);
    XCTAssertTrue(expectation3 == returned3,@"Process Input #2 returned %d but %d was expected.",expectation3,returned3);
    
}

- (void)testExampleFour {

    // Testing provided example 4
    
    NSString * example = @"@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016";
    [_viewController processInputText:example];
    
    int expectation1 = 2;
    int returned1 = [_viewController.mentions count];
    
    int expectation2 = 1;
    int returned2 = [_viewController.emoticons count];
    
    int expectation3 = 1;
    int returned3 = [_viewController.links count];
    
    XCTAssertTrue(expectation1 == returned1,@"Process Input #1 returned %d but %d was expected.",expectation1,returned1);
    XCTAssertTrue(expectation2 == returned2,@"Process Input #2 returned %d but %d was expected.",expectation2,returned2);
    XCTAssertTrue(expectation3 == returned3,@"Process Input #2 returned %d but %d was expected.",expectation3,returned3);

}


@end
